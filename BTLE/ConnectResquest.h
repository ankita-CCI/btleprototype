//
//  ConnectResquest.h
//  BTLE
//
//  Created by Ankita Kalangutkar on 08/01/14.
//  Copyright (c) 2014 creative capsule. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ConnectResquest : NSObject

@property (nonatomic,strong) NSString *UUID;

+( RKObjectMapping *)defineMapping;
@end
