//
//  ConnectResponse.h
//  BTLE
//
//  Created by Ankita Kalangutkar on 08/01/14.
//  Copyright (c) 2014 creative capsule. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface ConnectResponse : NSObject
//@property (nonatomic,strong)	NSString	*personID;
//@property (nonatomic,strong) NSString * firstName;
//@property (nonatomic,strong) NSString * lastName;

@property (nonatomic,strong)	NSString	*latitude;
@property (nonatomic,strong) NSString * longitude;
@property (nonatomic,strong) NSString * zoom;
@property (nonatomic,strong) NSString * address;

+ (RKObjectMapping *) defineMapping;

@end
