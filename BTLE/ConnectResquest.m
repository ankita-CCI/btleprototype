//
//  ConnectResquest.m
//  BTLE
//
//  Created by Ankita Kalangutkar on 08/01/14.
//  Copyright (c) 2014 creative capsule. All rights reserved.
//

#import "ConnectResquest.h"

@implementation ConnectResquest
+ (RKObjectMapping *) defineMapping
{
    RKObjectMapping *connectionResquestMapping = [RKObjectMapping requestMapping];
    
//    [connectionResquestMapping addAttributeMappingsFromArray:@[@"UUID"]];
    [connectionResquestMapping addAttributeMappingsFromDictionary:@{@"UUID": @"uuid"
															   }];
    
//    [connectionResquestMapping addAttributeMappingsFromDictionary:@{@"": @""
//     }];
    
    return connectionResquestMapping;
}
@end
