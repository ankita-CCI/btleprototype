//
//  SharedManager.m
//  BTLE
//
//  Created by Ankita Kalangutkar on 08/01/14.
//  Copyright (c) 2014 creative capsule. All rights reserved.
//

#import "SharedManager.h"
#import "ConnectResquest.h"
#import "ConnectResponse.h"

static SharedManager *sharedSearchManager = nil;

@implementation SharedManager

+ (id) sharedSearchManager {
	@synchronized(sharedSearchManager) {
		sharedSearchManager = [[super alloc] init];
		
//		[self initializeObjectManagerSettings];
	}
	return sharedSearchManager;
}


-(id) init
{
    self = [super init];
    if (self)
    {
        RKObjectManager *restkitManager = [RKObjectManager managerWithBaseURL:[[NSURL alloc] initWithString:@"http://localhost:8888/"]];
        [RKObjectManager setSharedManager:restkitManager];
        
        RKObjectMapping * connectionRequest = [ConnectResquest defineMapping];
        RKRequestDescriptor *connectionRequestDescriptor = [RKRequestDescriptor requestDescriptorWithMapping:connectionRequest objectClass:[ConnectResquest class] rootKeyPath:nil method:RKRequestMethodPOST];
        [restkitManager addRequestDescriptor:connectionRequestDescriptor];
        
        RKObjectMapping * connectResponse = [ConnectResponse defineMapping];
        RKResponseDescriptor *connectionResponseDescrptor = [RKResponseDescriptor responseDescriptorWithMapping:connectResponse method:RKRequestMethodPOST pathPattern:[[[NSURL alloc] initWithString:@"dbcall.php"] path] keyPath:nil statusCodes:RKStatusCodeIndexSetForClass(RKStatusCodeClassSuccessful)];
        
        [restkitManager addResponseDescriptor:connectionResponseDescrptor];
    }
    return self;
}
@end
