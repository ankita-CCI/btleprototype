//
//  CentralViewController.h
//  BTLE
//
//  Created by Ankita Kalangutkar on 03/01/14.
//  Copyright (c) 2014 creative capsule. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreBluetooth/CoreBluetooth.h>

@interface CentralViewController : UIViewController

-(void) reInstantiateCentralManager:(NSArray *) optionsArray;

- (IBAction)btnclick:(id)sender;

@end
