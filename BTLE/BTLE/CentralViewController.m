//
//  CentralViewController.m
//  BTLE
//
//  Created by Ankita Kalangutkar on 03/01/14.
//  Copyright (c) 2014 creative capsule. All rights reserved.
//

#import "CentralViewController.h"
#import "TransferService.h"

#import "ConnectResponse.h"

//#define kBaseURL @"http://localhost:8888/"
//#define kFetchURL @"dbcall.php"
//#define kEntireURL @"http://localhost:8888/dbcall.php"

#define kBaseURL @"http://api.wipmania.com/"
#define kFetchURL @"json"
#define kEntireURL @"http://api.wipmania.com/json"

@interface CentralViewController () <CBCentralManagerDelegate, CBPeripheralDelegate>
@property (nonatomic,strong) CBCentralManager *myCentralManager;
@property (nonatomic,strong) CBPeripheral *connectingPeripheral;
@property (strong, nonatomic) NSMutableData *data;
@property (strong, nonatomic) IBOutlet UITextView *textView;
@property (strong, nonatomic) IBOutlet UIImageView *imageView;

@end

@implementation CentralViewController


- (void)viewDidLoad
{
    [super viewDidLoad];
    // Start up the CBCentralManager
    self.myCentralManager = [[CBCentralManager alloc] initWithDelegate:self queue:nil];
    self.data = [[NSMutableData alloc] init];
}


- (void)viewWillDisappear:(BOOL)animated
{
  
    
    [super viewWillDisappear:animated];
    
    // Don't keep it going while we're not showing.
    [self.myCentralManager stopScan];
    NSLog(@"Scanning stopped");
}

- (IBAction)btnclick:(id)sender{
    self.textView.text = @"";
    
}

#pragma mark - Central Methods

-(void) centralManager:(CBCentralManager *)central willRestoreState:(NSDictionary *)dict // similar deleagtion method for peripheral
{
    // list of al the peripherals central wasconnected to
  //  NSArray *peripherals = dict [CBCentralManagerRestoredStatePeripheralsKey];
    //TO DO be sure to set a peripheral’s delegate to ensure that it receives the appropriate callbacks
}


/** centralManagerDidUpdateState is a required protocol method.
 *  Usually, you'd check for other states to make sure the current device supports LE, is powered on, etc.
 *  In this instance, we're just using it to wait for CBCentralManagerStatePoweredOn, which indicates
 *  the Central is ready to be used.
 */
//For apps that don’t opt in to state preservation (or if there is nothing to restore upon launch), the centralManagerDidUpdateState: and peripheralManagerDidUpdateState: delegate methods are invoked first instead.
- (void)centralManagerDidUpdateState:(CBCentralManager *)central
{
    //    if (central.state != CBCentralManagerStatePoweredOn) {
    //        // In a real app, you'd deal with all the states correctly
    //        return;
    //    }
    
    // The state must be CBCentralManagerStatePoweredOn...
    
    //it can be important in ensuring that things run smoothly in your app
    NSUInteger serviceUUIDIndex =
    
    [self.connectingPeripheral.services indexOfObjectPassingTest:^BOOL(CBService *obj,
                                                        
                                                        NSUInteger index, BOOL *stop) {
        
        return [obj.UUID isEqual:TRANSFER_SERVICE_UUID];
        
    }];
    
    if (serviceUUIDIndex == NSNotFound)
    {
        [self.connectingPeripheral discoverServices:@[TRANSFER_SERVICE_UUID]];
    }
    
    switch (central.state)
    {
        case CBCentralManagerStatePoweredOn:
            [self scan];
            break;
        case CBCentralManagerStatePoweredOff:
            NSLog(@"CBCentralManagerStatePoweredOff");
            break;
        case CBCentralManagerStateResetting:
            NSLog(@"CBCentralManagerStateResetting");
            break;
        case CBCentralManagerStateUnauthorized:
            NSLog(@"CBCentralManagerStateUnauthorized");;
            break;
        case CBCentralManagerStateUnknown:
            NSLog(@"CBCentralManagerStateUnknown");
            break;
        case CBCentralManagerStateUnsupported:
            NSLog(@"CBCentralManagerStateUnsupported");
            break;
            
        default:
            break;
    }
    
    // ... so start scanning
    
    
}


/** Scan for peripherals - specifically for our service's 128bit CBUUID
 */
- (void)scan
{
    [self.myCentralManager scanForPeripheralsWithServices:@[[CBUUID UUIDWithString:TRANSFER_SERVICE_UUID]]
                                                options:@{ CBCentralManagerScanOptionAllowDuplicatesKey : @YES }];
    
    NSLog(@"Scanning started");
}


/** This callback comes whenever a peripheral that is advertising the TRANSFER_SERVICE_UUID is discovered.
 *  We check the RSSI, to make sure it's close enough that we're interested in it, and if it is,
 *  we start the connection process
 */
- (void)centralManager:(CBCentralManager *)central didDiscoverPeripheral:(CBPeripheral *)peripheral advertisementData:(NSDictionary *)advertisementData RSSI:(NSNumber *)RSSI
{
    // Reject any where the value is above reasonable range
    if (RSSI.integerValue > -15) {
        return;
    }
    
    // Reject if the signal strength is too low to be close enough (Close is around -22dB)
    if (RSSI.integerValue < -35) {
        return;
    }
    
    NSLog(@"Discovered %@ at %@", peripheral.name, RSSI);
    
    // Ok, it's in range - have we already seen it?
    if (self.connectingPeripheral != peripheral) {
        
        // Save a local copy of the peripheral, so CoreBluetooth doesn't get rid of it
        self.connectingPeripheral = peripheral;
        
        // And connect
        NSLog(@"Connecting to peripheral %@", peripheral);
        [self.myCentralManager connectPeripheral:peripheral options:@{CBConnectPeripheralOptionNotifyOnConnectionKey:@YES,CBConnectPeripheralOptionNotifyOnDisconnectionKey:@YES,CBConnectPeripheralOptionNotifyOnNotificationKey:@YES}];
        self.connectingPeripheral = peripheral;
    }
}


/** If the connection fails for whatever reason, we need to deal with it.
 */
- (void)centralManager:(CBCentralManager *)central didFailToConnectPeripheral:(CBPeripheral *)peripheral error:(NSError *)error
{
    NSLog(@"Failed to connect to %@. (%@)", peripheral, [error localizedDescription]);
    [self cleanup];
}


/** We've connected to the peripheral, now we need to discover the services and characteristics to find the 'transfer' characteristic.
 */
- (void)centralManager:(CBCentralManager *)central didConnectPeripheral:(CBPeripheral *)peripheral
{
    NSLog(@"Peripheral Connected");
    
    // Stop scanning
    [self.myCentralManager stopScan];
    NSLog(@"Scanning stopped");
    
    // Clear the data that we may already have
    [self.data setLength:0];
    
    // Make sure we get the discovery callbacks
    peripheral.delegate = self;
    
    // Search only for services that match our UUID
    [peripheral discoverServices:@[[CBUUID UUIDWithString:TRANSFER_SERVICE_UUID]]];
}


/** The Transfer Service was discovered
 */
- (void)peripheral:(CBPeripheral *)peripheral didDiscoverServices:(NSError *)error
{
    if (error) {
        NSLog(@"Error discovering services: %@", [error localizedDescription]);
        [self cleanup];
        return;
    }
    
    // Discover the characteristic we want...
    
    // Loop through the newly filled peripheral.services array, just in case there's more than one.
    for (CBService *service in peripheral.services) {
        [peripheral discoverCharacteristics:@[[CBUUID UUIDWithString:TRANSFER_CHARACTERISTIC_UUID]] forService:service];
    }
}


/** The Transfer characteristic was discovered.
 *  Once this has been found, we want to subscribe to it, which lets the peripheral know we want the data it contains
 */
- (void)peripheral:(CBPeripheral *)peripheral didDiscoverCharacteristicsForService:(CBService *)service error:(NSError *)error
{
    // Deal with errors (if any)
    if (error) {
        NSLog(@"Error discovering characteristics: %@", [error localizedDescription]);
        [self cleanup];
        return;
    }
    
    // Again, we loop through the array, just in case.
    for (CBCharacteristic *characteristic in service.characteristics) {
        
        // And check if it's the right one
        if ([characteristic.UUID isEqual:[CBUUID UUIDWithString:TRANSFER_CHARACTERISTIC_UUID]]) {
            
            // If it is, subscribe to it
            [peripheral setNotifyValue:YES forCharacteristic:characteristic];
        }
    }
    
    // Once this is complete, we just need to wait for the data to come in.
}


/** This callback lets us know more data has arrived via notification on the characteristic
 */
- (void)peripheral:(CBPeripheral *)peripheral didUpdateValueForCharacteristic:(CBCharacteristic *)characteristic error:(NSError *)error
{
    if (error) {
        NSLog(@"Error discovering characteristics: %@", [error localizedDescription]);
        return;
    }
    
    NSString *stringFromData = [[NSString alloc] initWithData:characteristic.value encoding:NSUTF8StringEncoding];
    
    // Have we got everything we need?
    if ([stringFromData isEqualToString:@"EOM"])
    {
        // We have, so show the data,
        [self.textView setText:[[NSString alloc] initWithData:self.data encoding:NSUTF8StringEncoding]];
        
         // post to the webservice
        [self initREST];
    
        // Cancel our subscription to the characteristic
        [peripheral setNotifyValue:NO forCharacteristic:characteristic];
        
        // and disconnect from the peripehral
        [self.myCentralManager cancelPeripheralConnection:peripheral];
    }
    
    // Otherwise, just add the data on to what we already have
    [self.data appendData:characteristic.value];
    
    // Log it
    NSLog(@"Received: %@", stringFromData);
    
}


/** The peripheral letting us know whether our subscribe/unsubscribe happened or not
 */
- (void)peripheral:(CBPeripheral *)peripheral didUpdateNotificationStateForCharacteristic:(CBCharacteristic *)characteristic error:(NSError *)error
{
    if (error) {
        NSLog(@"Error changing notification state: %@", error.localizedDescription);
    }
    
    // Exit if it's not the transfer characteristic
    if (![characteristic.UUID isEqual:[CBUUID UUIDWithString:TRANSFER_CHARACTERISTIC_UUID]]) {
        return;
    }
    
    // Notification has started
    if (characteristic.isNotifying)
    {
        NSLog(@"Notification began on %@", characteristic);
    }
    
    // Notification has stopped
    else {
        // so disconnect from the peripheral
        NSLog(@"Notification stopped on %@.  Disconnecting", characteristic);
        [self.myCentralManager cancelPeripheralConnection:peripheral];
    }
}


/** Once the disconnection happens, we need to clean up our local copy of the peripheral
 */
- (void)centralManager:(CBCentralManager *)central didDisconnectPeripheral:(CBPeripheral *)peripheral error:(NSError *)error
{
    NSLog(@"Peripheral Disconnected");
   // self.connectingPeripheral = nil;
    
    // We're disconnected, so start scanning again
    [self scan];
}


/** Call this when things either go wrong, or you're done with the connection.
 *  This cancels any subscriptions if there are any, or straight disconnects if not.
 *  (didUpdateNotificationStateForCharacteristic will cancel the connection if a subscription is involved)
 */
- (void)cleanup
{
    // Don't do anything if we're not connected
    if (!self.connectingPeripheral.isConnected) {
        return;
    }
    
    // See if we are subscribed to a characteristic on the peripheral
    if (self.connectingPeripheral.services != nil) {
        for (CBService *service in self.connectingPeripheral.services) {
            if (service.characteristics != nil) {
                for (CBCharacteristic *characteristic in service.characteristics) {
                    if ([characteristic.UUID isEqual:[CBUUID UUIDWithString:TRANSFER_CHARACTERISTIC_UUID]]) {
                        if (characteristic.isNotifying) {
                            // It is notifying, so unsubscribe
                            [self.connectingPeripheral setNotifyValue:NO forCharacteristic:characteristic];
                            
                            // And we're done.
                            return;
                        }
                    }
                }
            }
        }
    }
    
    // If we've got this far, we're connected, but we're not subscribed, so we just disconnect
    [self.myCentralManager cancelPeripheralConnection:self.connectingPeripheral];
}

#pragma mark -- RESTKit methods
- (void) initREST
{
    RKObjectManager *manager = [RKObjectManager managerWithBaseURL:[[NSURL alloc] initWithString:kBaseURL]];
    [RKObjectManager setSharedManager:manager];
    
    RKObjectMapping * connectResponse = [ConnectResponse defineMapping];
    //    RKResponseDescriptor *connectionResponseDescrptor = [RKResponseDescriptor responseDescriptorWithMapping:connectResponse method:RKRequestMethodPOST pathPattern:[[[NSURL alloc] initWithString:@"dbcall.php"] path] keyPath:nil statusCodes:RKStatusCodeIndexSetForClass(RKStatusCodeClassSuccessful)];
    RKResponseDescriptor *connectionResponseDescrptor = [RKResponseDescriptor responseDescriptorWithMapping:connectResponse method:RKRequestMethodGET pathPattern:kFetchURL keyPath:nil statusCodes:RKStatusCodeIndexSetForClass(RKStatusCodeClassSuccessful)];
    
    [manager addResponseDescriptor:connectionResponseDescrptor];

    
    RKManagedObjectRequestOperation *operation = [manager appropriateObjectRequestOperationWithObject:nil method:RKRequestMethodGET path:kEntireURL parameters: nil];
    [operation setCompletionBlockWithSuccess:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult) {
        		NSLog(@"success");
        
        for (ConnectResponse *connectionResponse in [mappingResult array])
        {
            NSLog(@"connectionResponse %@", connectionResponse.latitude);
        }
        if ([[UIApplication sharedApplication] applicationState] == UIApplicationStateBackground)
        {
              [self presentNotification];
        }

        
    } failure:^(RKObjectRequestOperation *operation, NSError *error) {
        		NSLog(@"It Failed: %@", error);
    }];
    
    [RKObjectManager.sharedManager enqueueObjectRequestOperation:operation];
    
    // or
    
//    NSURL *URL = [NSURL URLWithString:kEntireURL];
//    
//    NSURLRequest *request = [NSURLRequest requestWithURL:URL];
//    RKObjectRequestOperation *objectRequestOperation = [[RKObjectRequestOperation alloc] initWithRequest:request responseDescriptors:@[ connectionResponseDescrptor ]];
//    [objectRequestOperation setCompletionBlockWithSuccess:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult) {
//        RKLogInfo(@"Load collection of Videos: %@", mappingResult.array);
//    } failure:^(RKObjectRequestOperation *operation, NSError *error) {
//        RKLogError(@"Operation failed with error: %@", error);
//    }];
//    
//    [objectRequestOperation start];

}

-(void) presentNotification
{
    UILocalNotification *localNotificationAlert = [[UILocalNotification alloc] init];
    localNotificationAlert.fireDate = [NSDate dateWithTimeIntervalSinceNow: 10];
    localNotificationAlert.alertBody = @"Webservice Call Complete";
    localNotificationAlert.alertAction = @"Background Transfer Complete!";
//    localNotificationAlert.applicationIconBadgeNumber = [[UIApplication sharedApplication] applicationIconBadgeNumber] + 1;
    [[UIApplication sharedApplication] presentLocalNotificationNow:localNotificationAlert];

}



@end
