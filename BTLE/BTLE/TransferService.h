//
//  TransferService.h
//  BTLE
//
//  Created by Ankita Kalangutkar on 03/01/14.
//  Copyright (c) 2014 creative capsule. All rights reserved.
//

#ifndef LE_Transfer_TransferService_h
#define LE_Transfer_TransferService_h

#define TRANSFER_CHARACTERISTIC_UUID @"4839A7C8-3C0D-43D1-BDA9-E68A4C0137B5"
#define TRANSFER_SERVICE_UUID @"ECA1013F-9148-4F73-B14A-25635A11D89B"


#define BACKGROUND   @"kAlarmServiceEnteredBackgroundNotification"
#define FOREGROUND  @"kAlarmServiceEnteredForegroundNotification"
#endif