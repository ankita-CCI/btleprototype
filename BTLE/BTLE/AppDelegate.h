//
//  AppDelegate.h
//  BTLE
//
//  Created by Ankita Kalangutkar on 03/01/14.
//  Copyright (c) 2014 creative capsule. All rights reserved.
//

#import <UIKit/UIKit.h>


@class ViewController;

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) ViewController *viewController;

@end
