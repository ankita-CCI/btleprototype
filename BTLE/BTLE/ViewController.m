//
//  ViewController.m
//  BTLE
//
//  Created by Ankita Kalangutkar on 03/01/14.
//  Copyright (c) 2014 creative capsule. All rights reserved.
//

#import "ViewController.h"
#import "CentralViewController.h"
#import "PeripheralViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)centralMode:(UIButton *)sender
{
    CentralViewController *centralViewController = [[CentralViewController alloc] initWithNibName:@"CentralViewController" bundle:nil];
    [self.navigationController pushViewController:centralViewController animated:YES];
}

- (IBAction)peripheralMode:(UIButton *)sender
{
    PeripheralViewController *peripheralViewController = [[PeripheralViewController alloc] initWithNibName:@"PeripheralViewController" bundle:nil];
    [self.navigationController pushViewController:peripheralViewController animated:YES];
}

@end
