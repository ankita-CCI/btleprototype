//
//  PeripheralViewController.h
//  BTLE
//
//  Created by Ankita Kalangutkar on 03/01/14.
//  Copyright (c) 2014 creative capsule. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreBluetooth/CoreBluetooth.h>

@interface PeripheralViewController : UIViewController <CBPeripheralManagerDelegate, UITextFieldDelegate>


@property (strong, nonatomic) IBOutlet UITextView *textView;

- (IBAction)swichChanged:(UISwitch *)sender;
@property (strong, nonatomic) IBOutlet UISwitch *advertisingSwitch;
@end
