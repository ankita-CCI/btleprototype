//
//  ConnectResponse.m
//  BTLE
//
//  Created by Ankita Kalangutkar on 08/01/14.
//  Copyright (c) 2014 creative capsule. All rights reserved.
//

#import "ConnectResponse.h"


@implementation ConnectResponse

//+ (RKObjectMapping *) defineMapping
//{
//    RKObjectMapping *connectionResponseMapping =  [RKObjectMapping mappingForClass:[ConnectResponse class] ];
//	
//    // second is class variables
//    
//	[connectionResponseMapping addAttributeMappingsFromDictionary:@{
//                                                               @"personID":@"personID",
//                                                               @"firstName":@"firstName",
//                                                               @"lastName":@"lastName"
//                                                               }];
//
//    return connectionResponseMapping;
//	
//}

+ (RKObjectMapping *) defineMapping
{
    RKObjectMapping *connectionResponseMapping =  [RKObjectMapping mappingForClass:[ConnectResponse class] ];
	
    // second is class variables
    
	[connectionResponseMapping addAttributeMappingsFromDictionary:@{
     @"latitude":@"latitude",
     @"longitude":@"longitude",
     @"zoom":@"zoom",
     @"address":@"address"
     }];
    
    return connectionResponseMapping;
	
}


@end
